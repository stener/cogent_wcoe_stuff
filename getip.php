<?php

function cls()
{
	echo "\x1Bc";
	echo "\x1B[3J";
};

function greenprint($string)
{
	echo "\x1B[1;40;92m";
	if (is_array($string))
	{
		print_r(array_values($string));
	}
	else
	{
		echo "$string";
	};
	
	echo "\x1B[0m";
};

function set_vars()
{
	$host = gethostname();
	$ip = gethostbyname(gethostname());
	return array($host,$ip);
};

cls();

#	print the hostname and IP address
if (! isset($hostinfo))
{
	$hostinfo=set_vars();
	greenprint($hostinfo['0'] . " has the IP address of: " . $hostinfo['1'] . "\n");
};

?>

